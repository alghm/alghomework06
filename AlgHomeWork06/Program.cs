﻿using System.Diagnostics;

var array_1_1000 = new int[1000];
var array_2_1000 = new int[1000];
var array_1_10_000 = new int[10_000];
var array_2_10_000 = new int[10_000];
var array_1_100_000 = new int[100_000];
var array_2_100_000 = new int[100_000];
var array_1_1000_000 = new int[1000_000];
var array_2_1000_000 = new int[1000_000];

for (int i = 0; i < 1000; i++)
{
    var number = new Random().Next(0, 1000);
	array_1_1000[i] = number;
	array_2_1000[i] = number;
}


for (int i = 0; i < 10_000; i++)
{
    var number = new Random().Next(0, 10_000);
    array_1_10_000[i] = number;
    array_2_10_000[i] = number;
}

for (int i = 0; i < 100_000; i++)
{
    var number = new Random().Next(0, 100_000);
	array_1_100_000[i] = number;
	array_2_100_000[i] = number;
}

for (int i = 0; i < 1000_000; i++)
{
    var number = new Random().Next(0, 1000_000);
    array_1_1000_000[i] = number;
    array_2_1000_000[i] = number;
}


Stopwatch stopwatch = new Stopwatch();


stopwatch.Start();
BubbleSort(array_1_1000);
stopwatch.Stop();
long elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("BubbleSort1 для 1000 - {0}", elapsedTime);

stopwatch.Restart();
BubbleSort(array_1_10_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("BubbleSort1 для 10 000 - {0}", elapsedTime);

stopwatch.Restart();
BubbleSort(array_1_100_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("BubbleSort1 для 100 000 - {0}", elapsedTime);
/*
stopwatch.Restart();
BubbleSort(array_1_1000_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("BubbleSort1 для 1 000 000 - {0}", elapsedTime);
*/

stopwatch.Restart();
BubbleSort2(array_2_1000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("BubbleSort2 для 1000 - {0}", elapsedTime);

stopwatch.Restart();
BubbleSort2(array_2_10_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("BubbleSort2 для 10 000 - {0}", elapsedTime);

stopwatch.Restart();
BubbleSort2(array_2_100_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("BubbleSort2 для 100 000 - {0}", elapsedTime);

/*stopwatch.Restart();
BubbleSort2(array_2_1000_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("BubbleSort2 для 1 000 000 - {0}", elapsedTime);*/


Console.ReadKey();



static void BubbleSort(int[] array)
{
	int length = array.Length;
    for (int j = length - 1; j > 0; j--)
	{
		for (int i = 0; i < j; i++)
		{
			if (array[i] > array[j])
			{
				int temp = array[i];
				array[i] = array[j];
				array[j] = temp;
			}
        }
	}
}

static void BubbleSort2(int[] array)
{
    for (int i = 0; i < array.Length; i++)
    {
        bool swapped = false;
        for (int j = 0; j < array.Length - i - 1; j++)
        {
            if (array[j] > array[j + 1])
            {
                int temp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = temp;

                swapped = true;
            }
        }

        if (!swapped)
        {
            break;
        }
    }
}


static void InsertionSort(int[] array)
{
    for (int i = 0; i < array.Length; i++)
    {
        int j;
        int val = array[i];

        for (j = i - 1; j >= 0;)
        {
            if (val < array[j])
            {
                array[j + 1] = array[j];
                j--;
                array[j + 1] = val;
            }
            else
            {
                break;
            }
        }
    }
}

static void InsertionSort2(int[] array)
{
    for (int i = 1; i < array.Length; i++)
    {
        int temp = array[i];
        int left = 0;
        int right = i - 1;

        while (left <= right)
        {
            int mid = (left + right) / 2;
            if (temp < array[mid])
                right = mid - 1;
            else
                left = mid + 1;
        }

        for (int j = i - 1; j >= left; j--)
            array[j + 1] = array[j];

        array[left] = temp;
    }
}

static void ShellSort(int[] array)
{
    for (int interval = array.Length / 2; interval > 0; interval /= 2)
    {
        for (int i = interval; i < array.Length; i++)
        {
            var currentKey = array[i];
            var k = i;

            while (k >= interval && array[k - interval] > currentKey)
            {
                array[k] = array[k - interval];
                k -= interval;
            }

            array[k] = currentKey;
        }
    }
}